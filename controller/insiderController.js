const { Insider } = require("../models/insiderModel");
const moment = require('moment');
const { Company } = require("../models/companyModel");

const addInsider = async (req, res) => {
    try {
        const compnayExist = await Company.findOne({ shortName: req.body.companyId });
        if (!compnayExist) return res.status(422).json({
            flag: false,
            data: 'Invalid companyId!'
        });

        // const updateDate = moment.utc().format();
        // const updateQuery = {}

        // if (req.body.table) {
        //     updateQuery['$push'] = { table: req.body.table };
        //     delete req.body.table
        // }

        // updateQuery['$set'] = { ...req.body, updateDate }
        req.body.companyId = compnayExist._id
        const insider = new Insider(req.body);
        // const updated = await Insider.({ companyId: req.body.companyId });
        const saved = await insider.save();
        return res.status(200).json({
            flag: true,
            data: saved
        });

    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const getInsider = async (req, res) => {
    try {

        const insiders = await Insider.find().populate('companyId')
        const mapped = insiders.map(insider => {
            const newData = {
                _id: insider._id,
                name: insider?.companyId?.name,
                shortName: insider?.companyId?.shortName,
                insiders: {
                    sharesSold: insider.sharesSold,
                    sharesBought: insider.sharesBought,
                    table: insider.table
                },
                createdAt: insider.createdAt,
                updateDate: insider.updateDate,
                updatedAt: insider.updatedAt,
            }

            return newData

        })
        return res.status(200).json({
            flag: true,
            data: mapped
        });

    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const getOneInsider = async (req, res) => {
    try {
        const companyId = req.params.companyId;

        const insider = await Company.findOne({ _id: companyId }).populate('insiders');

        return res.status(200).json({
            flag: true,
            data: insider
        });

    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

module.exports = {
    addInsider,
    getInsider,
    getOneInsider
}