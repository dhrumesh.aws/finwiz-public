const { Category, News } = require("../models/newsModel");
const { sendNotifications } = require("../services/notificationService");
const { User } = require("../models/userModel");
const { validateToken } = require('../services/jwtService');
const { Company } = require("../models/companyModel");
const moment = require('moment');

const addCategory = async (req, res) => {
    try {
        const categoryData = new Category(req.body);

        const category = await categoryData.save();

        return res.status(200).json({
            flag: true,
            data: category
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const editCategory = async (req, res) => {
    try {
        const categoryId = req.params.categoryId;

        const updatedCategory = await Category.findOneAndUpdate({ _id: categoryId }, { $set: req.body }, { new: true })

        return res.status(200).json({
            flag: true,
            data: updatedCategory
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const deleteCategory = async (req, res) => {
    try {
        const categoryId = req.params.categoryId;

        await Category.remove({ _id: categoryId });

        return res.status(200).json({
            flag: true,
            data: 'Category removed successfully!'
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}


const getAllCategory = async (req, res) => {
    try {
        const categories = await Category.find({});

        return res.status(200).json({
            flag: true,
            data: categories
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const addNews = async (req, res) => {
    try {
        const compnayExist = await Company.findOne({ shortName: req.body.companyId });
        if (!compnayExist) return res.status(422).json({
            flag: false,
            data: 'Invalid companyId!'
        });

        req.body.companyId = compnayExist._id
        const newsData = new News(req.body);

        const news = await newsData.save();

        sendNotifications('news', news.title, '');
        return res.status(200).json({
            flag: true,
            data: news
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const editNews = async (req, res) => {
    try {
        const newsId = req.params.newsId

        const updatedNews = await News.findOneAndUpdate({ _id: newsId }, { $set: req.body }, { new: true })

        return res.status(200).json({
            flag: true,
            data: updatedNews
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const deleteNews = async (req, res) => {
    try {
        const newsId = req.params.newsId

        await News.remove({ _id: newsId })

        return res.status(200).json({
            flag: true,
            data: 'News removed successfully!'
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const getAllNews = async (req, res) => {
    try {
        let user;

        const validate = await validateToken(req);
        if (validate.flag) user = validate.user;

        const options = { sort: { createdAt: -1 }, populate: ['companyId', 'categoryId'] }
        req.query.page ? options.page = req.query.page : {};
        req.query.limit ? options.limit = req.query.limit : {};
        const monthRecords = moment().add(-30, 'days').format()

        const dbQuery = req.query.categoryId ? { categoryId: req.query.categoryId, createdAt: { $gte: monthRecords } } : { createdAt: { $gte: monthRecords } }

        const newses = await News.paginate(dbQuery, options);

        const updated = user ? newses?.docs?.map(news => {
            const newsCopy = JSON.parse(JSON.stringify(news));
            newsCopy.isLiked = user?.likedNews?.includes(news._id);
            newsCopy.isFavourite = user?.favouriteNews?.includes(news._id);
            return newsCopy
        }) : newses.docs;


        return res.status(200).json({
            flag: true,
            data: updated
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const newsLike = async (req, res) => {
    try {
        const user = req.user;
        const { newsId, type } = req.body;
        if (user?.likedNews?.find(id => id.toString() === newsId) && type === 'like') return res.status(409).json({
            flag: false,
            message: 'This news already liked!'
        });

        if (!user?.likedNews?.find(id => id.toString() === newsId) && type !== 'like') return res.status(409).json({
            flag: false,
            message: 'This news already not liked!'
        });

        const userAction = type === 'like' ? { $push: { likedNews: newsId } } : { $pull: { likedNews: newsId } };
        const newsAction = type === 'like' ? { $inc: { likes: 1 } } : { $inc: { likes: -1 } };

        await User.findOneAndUpdate({ _id: user._id }, userAction, { new: true });
        const news = await News.findOneAndUpdate({ _id: newsId, $gte: 0 }, newsAction, { new: true });

        return res.status(200).json({
            flag: true,
            data: news
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const newsFavourite = async (req, res) => {
    try {
        const user = req.user;
        const { newsId, type } = req.body;
        if (user?.favouriteNews?.find(id => id.toString() === newsId) && type === 'favourite') return res.status(409).json({
            flag: false,
            message: 'This news already in favourite!'
        });

        if (!user?.favouriteNews?.find(id => id.toString() === newsId) && type !== 'favourite') return res.status(409).json({
            flag: false,
            message: 'This news already not in favourite!'
        });

        const userAction = type === 'favourite' ? { $push: { favouriteNews: newsId } } : { $pull: { favouriteNews: newsId } };

        await User.findOneAndUpdate({ _id: user._id }, userAction, { new: true });

        return res.status(200).json({
            flag: true,
            data: type === 'favourite' ? 'Added to favourite!' : 'Removed from favourite!'
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const getAllWishList = async (req, res) => {
    try {
        const user = req.user;

        const wishlist = await User.findOne({ _id: user._id }).populate('favouriteNews');

        return res.status(200).json({
            flag: true,
            data: wishlist
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const searchNews = async (req, res) => {
    try {
        let user;

        const validate = await validateToken(req);
        if (validate.flag) user = validate.user;

        const companyId = req.query.companyId;
        const page = req.query.page ?? 1;
        const limit = req.query.limit ?? 10;
        const regex = new RegExp(req.query.text);

        const dbQuery = {};
        if (companyId) dbQuery.companyId = companyId;

        if (req.query.text) {
            const companies = await Company.find({ shortName: { $regex: regex, $options: "i" } }).select('shortName');

            companies.length ? dbQuery.$or = [{ title: { $regex: regex, $options: "i" } }, { companyId: { $in: companies.map(e => e._id) } }] : dbQuery.title = { $regex: regex, $options: "i" };;
        }

        const newses = await News.paginate(dbQuery, { page, limit, sort: { createdAt: -1 }, populate: ['companyId', 'categoryId'] });

        const updated = user ? newses?.docs?.map(news => {
            const newsCopy = JSON.parse(JSON.stringify(news));
            newsCopy.isLiked = user?.likedNews?.includes(news._id);
            newsCopy.isFavourite = user?.favouriteNews?.includes(news._id);
            return newsCopy
        }) : newses?.docs;

        return res.status(200).json({
            flag: true,
            data: updated
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const stockNews = async (req, res) => {
    try {
        const user = req.user
        const page = req.query.page ?? 1;
        const limit = req.query.limit ?? 10;

        const dbQuery = {};
        dbQuery.companyId = { $in: user?.addedStocks || [] };

        const newses = await News.paginate(dbQuery, { page, limit, sort: { createdAt: -1 }, populate: ['companyId', 'categoryId'] });

        const updated = newses?.docs?.map(news => {
            const newsCopy = JSON.parse(JSON.stringify(news));
            newsCopy.isLiked = user?.likedNews?.includes(news._id);
            newsCopy.isFavourite = user?.favouriteNews?.includes(news._id);
            return newsCopy
        });

        return res.status(200).json({
            flag: true,
            data: updated
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

module.exports = {
    addCategory,
    editCategory,
    deleteCategory,
    getAllCategory,
    addNews,
    editNews,
    deleteNews,
    getAllNews,
    newsLike,
    newsFavourite,
    getAllWishList,
    searchNews,
    stockNews
}