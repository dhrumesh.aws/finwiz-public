const { Contact } = require("../models/contactModel");

const addContact = async (req, res) => {
    try {
        const contactData = new Contact(req.body);

        const contact = await contactData.save();

        return res.status(200).json({
            flag: true,
            data: contact
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const getContacts = async (req, res) => {
    try {
        const page = req.query.page ?? 1;
        const limit = req.query.limit ?? 10;

        const contacts = await Contact.paginate({}, { page, limit, sort: { createdAt: -1 } });

        return res.status(200).json({
            flag: true,
            data: contacts
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

module.exports = {
    addContact,
    getContacts
}