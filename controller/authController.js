const { User } = require('../models/userModel');

const { issueToken } = require('../services/jwtService');
const referralCodeGenerator = require('referral-code-generator')

const login = async (req, res) => {
    try {
        let user = await User.findOne({ phone: req.body.phone });

        if (!user) {
            if (req.body.refferalCode) {
                const refUser = await User.findOneAndUpdate({ refferalCode: req.body.refferalCode }, { $inc: { refferalCount: 1 } }, { new: true });
                if (!refUser) {
                    return res.status(400).json({
                        flag: false,
                        message: "Invalid refferal code!"
                    })
                }
            }
            req.body.refferalCode = referralCodeGenerator.alphaNumeric('lowercase', 5, 3)
            const newUser = new User(req.body);

            user = await newUser.save();
        } else {
            user.fcm_token = req.body.fcm_token;
            user = await user.save()
        }
        const token = issueToken(user._id);

        return res.status(200).json({
            flag: true,
            token,
            data: user
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

module.exports = {
    login
}