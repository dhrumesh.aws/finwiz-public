const { Company } = require("../models/companyModel");

const addCompany = async (req, res) => {
    try {
        const companyData = new Company(req.body);

        const compnay = await companyData.save();

        return res.status(200).json({
            flag: true,
            data: compnay
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const deleteCompany = async (req, res) => {
    try {
        const companyId = req.params.companyId;

        await Company.remove({ _id: companyId });

        return res.status(200).json({
            flag: true,
            data: 'Company removed successfully!'
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

module.exports = {
    addCompany,
    deleteCompany
}