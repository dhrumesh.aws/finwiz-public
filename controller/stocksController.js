const { Company } = require("../models/companyModel");
const { News } = require("../models/newsModel");
const { User } = require("../models/userModel");

const searchStocks = async (req, res) => {
    try {
        const regex = new RegExp(req.query.text);

        const stocks = await Company.find({ $or: [{ name: { $regex: regex, $options: "i" } }, { shortName: { $regex: regex, $options: "i" } }] }, { name: 1, shortName: 1, _id: 1, userImage: 1, createdAt: 1 });

        if (!stocks) return res.status(400).json({
            flag: false,
            message: 'User not found.'
        });

        return res.status(200).json({
            flag: true,
            data: stocks
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}
const addStock = async (req, res) => {
    try {
        const user = req.user;

        if (user.addedStocks.length >= 5 && user.refferalCount < 3) {
            return res.status(409).json({
                flag: false,
                message: 'Please complete 3 refferals to add more than 5 stocks'
            });
        }
        if (user?.addedStocks?.find(comp => comp.toString() === req.body.stockId.toString())) {
            return res.status(409).json({
                flag: false,
                message: 'This stock already added!'
            });
        }
        const stocks = await User.findOneAndUpdate({ _id: user._id }, { $addToSet: { addedStocks: req.body.stockId } }, { new: true });

        if (!stocks) return res.status(400).json({
            flag: false,
            message: 'User not found!'
        });

        return res.status(200).json({
            flag: true,
            data: 'Stock Added successfully!'
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const summary = async (req, res) => {
    try {
        const user = req.user;

        const company = await Company.find({ _id: { $in: user.addedStocks } })

        const userStocks = await News.aggregate([
            {
                $match: {
                    companyId: {
                        $in: company.map(comp => comp._id)
                    }
                },
            },
            {
                $group: {
                    _id: { companyId: "$companyId", type: "$type" },
                    count: { $sum: 1 }
                },
            }
        ]);

        const final = company.map(comp => {
            const companyCopy = JSON.parse(JSON.stringify(comp))
            companyCopy.positive = userStocks.find(e => e?._id?.type === 1 && e?._id?.companyId.toString() === companyCopy?._id?.toString())?.count ?? 0
            companyCopy.negative = userStocks.find(e => e?._id?.type === -1 && e?._id?.companyId.toString() === companyCopy?._id?.toString())?.count ?? 0
            return companyCopy
        })

        return res.status(200).json({
            flag: true,
            data: final
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const removeStocks = async (req, res) => {
    try {
        const user = req.user;

        const stocks = await User.findOneAndUpdate({ _id: user._id }, { $pull: { addedStocks: { $in: req.body.stockId } } }, { new: true });

        if (!stocks) return res.status(400).json({
            flag: false,
            message: 'User not found!'
        });

        return res.status(200).json({
            flag: true,
            data: 'Stock Removed successfully!'
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const getUserStocks = async (req, res) => {
    try {
        const options = { sort: { createdAt: -1 }, populate: ['addedStocks'], select: 'name userImage addedStocks createdAt phone' }
        req.query.page ? options.page = req.query.page : {};
        req.query.limit ? options.limit = req.query.limit : {};

        const users = await User.paginate({}, options)
        return res.status(200).json({
            flag: true,
            data: users
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}
module.exports = {
    searchStocks,
    addStock,
    summary,
    removeStocks,
    getUserStocks
}