const { Notification } = require("../models/notificationModel");
const { broadcastNotifications } = require('../services/notificationService')


const getNotifications = async (req, res) => {
    try {
        const user = req.user;

        const notifications = await Notification.find({ userId: user._id }).sort({ createdAt: -1 });

        return res.status(200).json({
            flag: true,
            message: notifications
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const broadcast = async (req, res) => {
    try {
        const data = req.body;
        await broadcastNotifications(data);
        return res.status(200).json({
            flag: true,
            message: 'Notification sent to all users!'
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

module.exports = {
    getNotifications,
    broadcast
}