const { Company } = require("../models/companyModel");
const { Movers, LatestMovers } = require("../models/moversModel");
const { User } = require("../models/userModel");
const { validateToken } = require('../services/jwtService');
const { sendNotifications } = require("../services/notificationService");

const addMovers = async (req, res) => {
    try {
        const compnayExist = await Company.findOne({ shortName: req.body.companyId });
        if (!compnayExist) return res.status(422).json({
            flag: false,
            data: 'Invalid companyId!'
        });

        req.body.companyId = compnayExist._id
        const moversData = new Movers(req.body);

        const movers = await moversData.save();

        sendNotifications('movers', movers.title, '')
        return res.status(200).json({
            flag: true,
            data: movers
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const editMovers = async (req, res) => {
    try {
        const moversId = req.params.moversId

        const updatedMovers = await Movers.findOneAndUpdate({ _id: moversId }, { $set: req.body }, { new: true })

        return res.status(200).json({
            flag: true,
            data: updatedMovers
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const deleteMovers = async (req, res) => {
    try {
        const moversId = req.params.moversId;

        await Movers.remove({ _id: moversId });

        return res.status(200).json({
            flag: true,
            data: 'Movers removed successfully!'
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const getAllMovers = async (req, res) => {
    try {
        let user;

        const validate = await validateToken(req);
        if (validate.flag) user = validate.user;

        const page = req.query.page ?? 1;
        const limit = req.query.limit ?? 10;

        const moverses = await Movers.paginate({}, { page, limit, sort: { createdAt: -1 }, populate: ['companyId'] });
        // const moversCopy = JSON.parse(JSON.stringify(moverses.docs));
        // const final = [];

        // for (let i = 0; i < moversCopy.length; i++) {
        //     const mvCopy = JSON.parse(JSON.stringify(moversCopy[i]))
        //     const lastRecord = await Movers.find({ _id: { $ne: mvCopy._id }, companyId: mvCopy?.companyId?._id, createdAt: { $lt: new Date(mvCopy.createdAt) } }).sort({ createdAt: -1 }).limit(1);
        //     mvCopy.priceRange = `${lastRecord?.[0]?.price ?? 0}-${mvCopy.price}`;
        //     mvCopy.signal = Number(mvCopy.price - (lastRecord?.[0]?.price ?? 0)) >= 0 ? 'up' : 'down';
        //     const percentage = ((mvCopy.price - lastRecord?.[0]?.price ?? 0) / lastRecord?.[0]?.price ?? 0) * 100;
        //     mvCopy.percentage = percentage ? percentage : 0;
        //     mvCopy.isLiked = user?.likedMovers?.includes(mvCopy._id);
        //     final.push(mvCopy);
        // }
        const updated = user ? moverses?.docs?.map(movers => {
            const mvCopy = JSON.parse(JSON.stringify(movers));
            mvCopy.isLiked = user?.likedMovers?.includes(movers._id);
            return mvCopy
        }) : moverses.docs;

        return res.status(200).json({
            flag: true,
            data: updated
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const moversLike = async (req, res) => {
    try {
        const user = req.user;
        const { moversId, type } = req.body;
        if (user?.likedMovers?.find(id => id.toString() === moversId) && type === 'like') return res.status(409).json({
            flag: false,
            message: 'This movers already liked!'
        });

        if (!user?.likedMovers?.find(id => id.toString() === moversId) && type !== 'like') return res.status(409).json({
            flag: false,
            message: 'This movers already not liked!'
        });

        const userAction = type === 'like' ? { $push: { likedMovers: moversId } } : { $pull: { likedMovers: moversId } };
        const moversAction = type === 'like' ? { $inc: { likes: 1 } } : { $inc: { likes: -1 } };

        await User.findOneAndUpdate({ _id: user._id }, userAction, { new: true });
        const movers = await Movers.findOneAndUpdate({ _id: moversId, likes: { $gte: 0 } }, moversAction, { new: true });

        return res.status(200).json({
            flag: true,
            data: movers
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const searchMovers = async (req, res) => {
    try {
        let user;

        const validate = await validateToken(req);
        if (validate.flag) user = validate.user;

        const companyId = req.query.companyId;
        const page = req.query.page ?? 1;
        const limit = req.query.limit ?? 10;
        const regex = new RegExp(req.query.text);

        const dbQuery = {};
        if (companyId) dbQuery._id = companyId;
        if (regex) dbQuery['$or'] = [{ name: { $regex: regex, $options: "i" } }, { shortName: { $regex: regex, $options: "i" } }];
        const companies = await Company.find(dbQuery);
        const moverses = await Movers.paginate({ companyId: { $in: companies } }, { page, limit, sort: { createdAt: -1 }, populate: ['companyId'] },);

        const updated = user ? moverses?.docs?.map(movers => {
            const mvCopy = JSON.parse(JSON.stringify(movers));
            mvCopy.isLiked = user?.likedMovers?.includes(movers._id);
            return mvCopy
        }) : moverses.docs;

        return res.status(200).json({
            flag: true,
            data: updated
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const addLatestMovers = async (req, res) => {
    try {
        // const compnayExist = await Company.findById(req.body.companyId);
        // if (!compnayExist) return res.status(422).json({
        //     flag: false,
        //     data: 'Invalid companyId!'
        // });

        const moversData = new LatestMovers(req.body);

        const movers = await moversData.save();

        return res.status(200).json({
            flag: true,
            data: movers
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const editLatestMovers = async (req, res) => {
    try {
        const moversId = req.params.moversId

        const updatedMovers = await LatestMovers.findOneAndUpdate({ _id: moversId }, { $set: req.body }, { new: true })

        return res.status(200).json({
            flag: true,
            data: updatedMovers
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const deleteLatestMovers = async (req, res) => {
    try {
        const moversId = req.params.moversId;

        await LatestMovers.remove({ _id: moversId });

        return res.status(200).json({
            flag: true,
            data: 'Movers removed successfully!'
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const getLatestMovers = async (req, res) => {
    try {
        const user = req.user;
        const page = req.query.page ?? 1;
        const limit = req.query.limit ?? 10;

        const moverses = await LatestMovers.paginate({}, { page, limit, sort: { createdAt: -1 } });

        return res.status(200).json({
            flag: true,
            data: moverses
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}
module.exports = {
    addMovers,
    editMovers,
    deleteMovers,
    getAllMovers,
    moversLike,
    searchMovers,
    addLatestMovers,
    editLatestMovers,
    deleteLatestMovers,
    getLatestMovers
}