const { User } = require('../models/userModel');

// const sendRefferal = async (req, res) => {
//     try {
//         const user = req.user;

//         const user = await User.findOneAndUpdate({ _id: user._id }, { $push: { pendingRefferals: } })
//     } catch (error) {

//     }
// }


const getUser = async (req, res) => {
    try {
        const user = req.user;

        return res.status(200).json({
            flag: true,
            data: user
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

const updateUser = async (req, res) => {
    try {
        const user = req.user;

        const updated = await User.findOneAndUpdate({ _id: user._id }, { $set: req.body }, { new: true });

        return res.status(200).json({
            flag: true,
            data: updated
        });
    } catch (error) {
        return res.status(500).json({
            flag: false,
            message: error.message
        });
    }
}

module.exports = {
    getUser,
    updateUser
}