var admin = require("firebase-admin");
var serviceAccount = require("../finwizz-app-be058-firebase-adminsdk.json");
const { Notification } = require("../models/notificationModel");
const { User } = require("../models/userModel");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})

const sendNotifications = async (type, company, title) => {
    try {
        let page = 0;
        let limit = 500;

        const message = {
            title: company,
            body: title,
        }
        const query = { fcm_token: { $exists: true } };
        switch (type) {
            case 'news':
                query.newsAlerts = true;
                break;
            case 'movers':
                query.portfolioAlerts = true;
                break;
            default:
                break;
        }

        while (true) {
            const users = await User.find(query, { fcm_token: 1 }).skip(page * limit).limit(limit);

            if (!users.length) break;

            const tokens = [];
            const records = [];

            users.forEach(user => {
                tokens.push(user.fcm_token)
                records.push({ title: message.title, body: message.body, userId: user._id });
            })
            // message.tokens = tokens;
            try {
                await admin.messaging().sendToDevice(tokens, {
                    notification: message
                });
                await Notification.insertMany(records);
            } catch (error) {
                console.log('messaging error', error);
            }
            page += 1;
        }

    } catch (error) {
        console.log(error.message)
    }
}

const broadcastNotifications = async (data) => {
    try {
        let page = 0;
        let limit = 500;

        const message = {
            title: data.title,
            body: data.body,
        }

        while (true) {
            const query = { fcm_token: { $exists: true } }
            if (data.users?.length) {
                query._id = { $in: data.users }
            }
            const users = await User.find(query, { fcm_token: 1 }).skip(page * limit).limit(limit);
            if (!users.length) break;

            const tokens = [];
            const records = [];

            users.forEach(user => {
                tokens.push(user.fcm_token)
                records.push({ title: message.title, body: message.body, userId: user._id });
            })
            try {
                await admin.messaging().sendToDevice(tokens, {
                    notification: message
                });
                await Notification.insertMany(records);
            } catch (error) {
                console.log('messaging error', error);
            }
            page += 1;
        }
    } catch (error) {
        return {
            flag: false,
            message: error
        }
    }
}
module.exports = {
    sendNotifications,
    broadcastNotifications
}