const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const companySchema = new Schema(
    {
        name: {
            type: String,
        },
        shortName: {
            type: String,
            unique: true
        }
    },
    {
        timestamps: true,
        versionKey: false,
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    },
);

companySchema.virtual('insiders', {
    ref: 'insider',
    localField: '_id',
    foreignField: 'companyId',
})

const Company = mongoose.model('company', companySchema);
module.exports = { Company }
