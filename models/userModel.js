const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate");

const userSchema = new Schema(
    {
        name: {
            type: String,
        },
        userImage: {
            type: String,
        },
        phone: {
            type: String,
            unique: true,
            required: true
        },
        fcm_token: {
            type: String,
            required: true
        },
        refferalCode: {
            type: String,
            required: true
        },
        newsAlerts: {
            type: Boolean,
            default: true
        },
        portfolioAlerts: {
            type: Boolean,
            default: true
        },
        likedNews: [{
            type: Schema.Types.ObjectId,
            ref: 'news'
        }],
        favouriteNews: [{
            type: Schema.Types.ObjectId,
            ref: 'news'
        }],
        likedMovers: [{
            type: Schema.Types.ObjectId,
            ref: 'movers'
        }],
        addedStocks: [{
            type: Schema.Types.ObjectId,
            ref: 'company'
        }],
        refferalCount: {
            type: Number,
            default: 0
        },
    },
    {
        timestamps: true,
        versionKey: false,
    },
).plugin(mongoosePaginate);

const User = mongoose.model('user', userSchema);
module.exports = { User }
