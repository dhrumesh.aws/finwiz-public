const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const moment = require('moment');

const insiderSchema = new Schema(
    {
        companyId: {
            type: Schema.Types.ObjectId,
            ref: 'company',
            required: true,
        },
        sharesSold: {
            shares: {
                type: Number
            },
            person: {
                type: Number
            }
        },
        sharesBought: {
            shares: {
                type: Number,
                default: 0
            },
            person: {
                type: Number,
                default: 0
            }
        },
        updateDate: {
            type: Date,
            default: moment(new Date()).format("YYYY-MM-DD[T]HH:mm:ss")
        },
        table: [{
            personCategory: {
                type: String,
            },
            shares: {
                type: Number,
                default: 0
            },
            value: {
                type: Number,
                default: 0
            },
            transactionType: {
                type: String
            },
            mode: {
                type: String
            }
        }]
    },
    {
        timestamps: true,
        versionKey: false,
    },
);

const Insider = mongoose.model('insider', insiderSchema);
module.exports = { Insider }
