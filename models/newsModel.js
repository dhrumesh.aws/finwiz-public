const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate");

const categorySchema = new Schema(
    {
        name: {
            type: String,
            unique: true
        }
    },
    {
        timestamps: true,
        versionKey: false,
    },
).plugin(mongoosePaginate);

const newsSchema = new Schema(
    {
        title: {
            type: String
        },
        description: {
            type: String
        },
        categoryId: {
            type: Schema.Types.ObjectId,
            ref: 'category',
            required: true
        },
        companyId: {
            type: Schema.Types.ObjectId,
            ref: 'company',
            required: true
        },
        source: {
            type: String
        },
        type: {
            type: Number,
            enum: [-1, 0, 1], // -1 = negative, 0 = neutral, 1 = positive
            default: 0
        },
        likes: {
            type: Number,
            default: 0
        },
        generic: {
            type: Boolean,
            default:false
        }
    },
    {
        timestamps: true,
        versionKey: false,
    },
).plugin(mongoosePaginate);

const News = mongoose.model('news', newsSchema);
const Category = mongoose.model('category', categorySchema);

module.exports = { News, Category }
