const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate");

const moversSchema = new Schema(
    {
        // title: {
        //     type: String
        // },
        // description: {
        //     type: String
        // },
        companyId: {
            type: Schema.Types.ObjectId,
            ref: 'company',
            required: true
        },
        source: {
            type: String
        },
        percentage: {
            type: Number,
            default: 0,
        },
        startDate: {
            type: String
        },
        endDate: {
            type: String
        },
        startPrice: {
            type: Number,
            default: 0,
        },
        currentPrice: {
            type: Number,
            default: 0,
        },
        type: {
            type: Number,
            enum: [-1, 0, 1], // -1 = negative, 0 = neutral, 1 = positive
            default: 0
        },
        likes: {
            type: Number,
            default: 0
        },
        imageType: {
            type: Number,
            default: 0
        }
    },
    {
        timestamps: true,
        versionKey: false,
    },
).plugin(mongoosePaginate);

const latestMoversSchema = new Schema(
    {
        title: {
            type: String
        },
        description: {
            type: String
        },
        // companyId: {
        //     type: Schema.Types.ObjectId,
        //     ref: 'company',
        //     required: true
        // },
        isActive: {
            type: Boolean,
            default: false
        },
        percentage: {
            type: Number,
            default: 0,
        },
        startDate: {
            type: String
        },
        endDate: {
            type: String
        },
    },
    {
        timestamps: true,
        versionKey: false,
    },
).plugin(mongoosePaginate);;

const Movers = mongoose.model('movers', moversSchema);
const LatestMovers = mongoose.model('latesmovers', latestMoversSchema);

module.exports = { Movers, LatestMovers }
