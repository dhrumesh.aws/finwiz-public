const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate");

const notificationSchema = new Schema(
    {
        title: {
            type: String
        },
        body: {
            type: String
        },
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
    },
    {
        timestamps: true,
        versionKey: false,
    },
).plugin(mongoosePaginate);

const Notification = mongoose.model('notification', notificationSchema);
module.exports = { Notification }
