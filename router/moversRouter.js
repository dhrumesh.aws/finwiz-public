const moversRouter = require('express').Router();

const { verifyToken } = require('../services/jwtService');

const { addMovers, editMovers, getAllMovers, moversLike, deleteMovers, searchMovers, addLatestMovers, editLatestMovers, deleteLatestMovers, getLatestMovers } = require("../controller/moversController");

moversRouter.post('/', addMovers);
moversRouter.patch('/:moversId', editMovers);
moversRouter.delete('/:moversId', deleteMovers);
moversRouter.get('/', getAllMovers);
moversRouter.post('/like', verifyToken, moversLike);
moversRouter.get('/search', searchMovers);
moversRouter.post('/latest', addLatestMovers);
moversRouter.patch('/latest/:moversId', editLatestMovers);
moversRouter.delete('/latest/:moversId', deleteLatestMovers);
moversRouter.get('/latest', verifyToken, getLatestMovers);

module.exports = { moversRouter };
