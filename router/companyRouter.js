const companyRouter = require('express').Router();

const { addCompany, deleteCompany } = require("../controller/companyController");

companyRouter.post('/', addCompany);
companyRouter.delete('/:companyId', deleteCompany);

module.exports = { companyRouter };
