const mainRouter = require('express').Router();

const { verifyToken } = require('../services/jwtService');

const { companyRouter } = require('./companyRouter');
const { newsRouter } = require('./newsRouter');
const { moversRouter } = require('./moversRouter');
const { authRouter } = require('./authRouter');
const { stocksRouter } = require('./stocksRouter');
const { userRouter } = require('./userRouter');
const { insiderRouter } = require('./insiderRouter');
const { contactRouter } = require('./contactRouter');
const { notificationRouter } = require('./notificationRouter');
// const { authRouter } = require('./authRouter');


mainRouter.use('/company', companyRouter);
mainRouter.use('/news', newsRouter);
mainRouter.use('/movers', moversRouter);
mainRouter.use('/auth', authRouter);
mainRouter.use('/stocks', stocksRouter);
mainRouter.use('/insider', insiderRouter);
mainRouter.use('/contact', contactRouter);
mainRouter.use('/notification', notificationRouter);
mainRouter.use('/user', verifyToken, userRouter);


module.exports = { mainRouter };