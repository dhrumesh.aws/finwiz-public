const stocksRouter = require('express').Router();

const { verifyToken } = require('../services/jwtService');

const { searchStocks, addStock, summary, removeStocks, getUserStocks } = require("../controller/stocksController");

stocksRouter.get('/search', verifyToken, searchStocks);
stocksRouter.post('/', verifyToken, addStock);
stocksRouter.get('/summary', verifyToken, summary);
stocksRouter.post('/remove', verifyToken, removeStocks);
stocksRouter.get('/users', verifyToken, getUserStocks);

module.exports = { stocksRouter };
