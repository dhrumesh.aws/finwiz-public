const authRouter = require('express').Router();

const { login } = require("../controller/authController");

authRouter.post('/login', login);

module.exports = { authRouter };
