const insiderRouter = require('express').Router();

const { addInsider, getInsider, getOneInsider } = require("../controller/insiderController");

insiderRouter.post('/', addInsider);
insiderRouter.get('/', getInsider);
insiderRouter.get('/:companyId', getOneInsider);

module.exports = { insiderRouter };
