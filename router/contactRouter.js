const contactRouter = require('express').Router();

const { addContact, getContacts } = require("../controller/contactController");

contactRouter.post('/', addContact);
contactRouter.get('/', getContacts);

module.exports = { contactRouter };
