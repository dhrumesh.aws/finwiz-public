const userRouter = require('express').Router();

const { getUser, updateUser } = require("../controller/userController");

userRouter.get('/', getUser);
userRouter.patch('/', updateUser);
// userRouter.get('/', getuserRecord);

module.exports = { userRouter };
