const notificationRouter = require('express').Router();

const { verifyToken } = require('../services/jwtService');

const { getNotifications, broadcast } = require("../controller/notificationController");

notificationRouter.get('/', verifyToken,getNotifications);
notificationRouter.post('/broadcast', broadcast);

module.exports = { notificationRouter };
