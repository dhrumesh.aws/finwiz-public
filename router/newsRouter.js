const newsRouter = require('express').Router();

const { verifyToken } = require('../services/jwtService');

const { addCategory, getAllCategory, addNews, editNews, getAllNews, newsLike, newsFavourite, getAllWishList, searchNews, deleteNews, deleteCategory, stockNews, editCategory } = require("../controller/newsController");

newsRouter.post('/category', addCategory);
newsRouter.patch('/category/:categoryId', editCategory);
newsRouter.delete('/category/:categoryId', deleteCategory);
newsRouter.get('/category', getAllCategory);
newsRouter.post('/', addNews);
newsRouter.patch('/:newsId', editNews);
newsRouter.delete('/:newsId', deleteNews);
newsRouter.get('/', getAllNews);
newsRouter.post('/like', verifyToken, newsLike);
newsRouter.post('/favourite', verifyToken, newsFavourite);
newsRouter.get('/wishlist', verifyToken, getAllWishList);
newsRouter.get('/search', searchNews);
newsRouter.get('/stockNews', verifyToken, stockNews);

module.exports = { newsRouter };
